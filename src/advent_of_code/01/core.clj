(ns advent-of-code.01.core
  (:require [advent-of-code.util :refer :all]))




(def input (clojure.string/split-lines (slurp "resources/01/01_input.txt")))

(defn fuel-for-w [weight]
  (int (- (Math/floor (/ weight 3)) 2)))

(defn fuel-for-module [module-weight]
  (let [initial-fuel (fuel-for-w module-weight)]
    (loop [total-fuel initial-fuel
           addl-fuel (fuel-for-w initial-fuel)]
      (if (pos? addl-fuel)
        (recur (+ total-fuel addl-fuel) (fuel-for-w addl-fuel))
        total-fuel))))

(defn fuel-to-launch-n [module-weights]
  (->> module-weights
       (map parse-int)
       (map fuel-for-module)
       (reduce +)))


