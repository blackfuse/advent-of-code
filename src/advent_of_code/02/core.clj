(ns advent-of-code.02.core
  (:require [advent-of-code.util :refer :all]))

(defn sort-map-to-list [map]
  (vals (into (sorted-map) map)))

(defn run-intcode [program-list]
  (let [program-map (zipmap (iterate inc 0) program-list)]
    (loop [pointer 0
           tape program-map]
      (let [op-code (get tape pointer)
            out-reg-pos (get tape (+ pointer 3))
            a-reg-pos (get tape (+ pointer 1))
            b-reg-pos (get tape (+ pointer 2))]
       (case op-code
         1 (recur (+ pointer 4) (assoc tape out-reg-pos (+ (get tape a-reg-pos) (get tape b-reg-pos))))
         2 (recur (+ pointer 4) (assoc tape out-reg-pos (* (get tape a-reg-pos) (get tape b-reg-pos))))
         99 (sort-map-to-list tape))))))

(def gravity-program (map parse-int (clojure.string/split (slurp "resources/02/02_input.txt") #",")))

(defn build-program [noun verb]
  (let [program-map (zipmap (iterate inc 0) gravity-program)
        gen-map (-> program-map
                    (assoc 1 noun)
                    (assoc 2 verb))]
    (sort-map-to-list gen-map)))

(defn find-inputs [target]
  (for [noun (range 100)
        verb (range 100)
        :when (= target (first (run-intcode (build-program noun verb))))]
    (println "noun" noun "verb" verb)))