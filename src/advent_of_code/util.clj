(ns advent-of-code.util)

(defn parse-int [str] (Integer/parseInt str))