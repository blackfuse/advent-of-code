(ns advent-of-code.01.core-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.01.core :refer [fuel-for-module fuel-for-w]]))

(deftest fuel-for-module-test
  (is (= 50346 (fuel-for-module 100756)))
  (is (= 966 (fuel-for-module 1969))))

(deftest fuel-for-w-test
  (is (= 2 (fuel-for-w 12)))
  (is (= 2 (fuel-for-w 14)))
  (is (= 654 (fuel-for-w 1969)))
  (is (= 33583 (fuel-for-w 100756))))