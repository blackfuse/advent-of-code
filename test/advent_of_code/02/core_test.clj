(ns advent-of-code.02.core-test
  (:require [clojure.test :refer :all])
  (:require [advent-of-code.02.core :refer [run-intcode]]))

(deftest run-program-test
  (is (= [3500,9,10,70,2,3,11,0,99,30,40,50] (run-intcode [1,9,10,3,2,3,11,0,99,30,40,50])))
  (is (= [2,0,0,0,99] (run-intcode [1,0,0,0,99])))
  (is (= [2,3,0,6,99] (run-intcode [2,3,0,3,99])))
  (is (= [2,4,4,5,99,9801] (run-intcode [2,4,4,5,99,0])))
  (is (= [30,1,1,4,2,5,6,0,99] (run-intcode [1,1,1,4,99,5,6,0,99]))))


